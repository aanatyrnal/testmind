package workers

import (
	"encoding/json"
	"fmt"
	"github.com/aanatyrnal/mind/config"
	"github.com/aanatyrnal/mind/models"
	"github.com/aanatyrnal/mind/services"
)

func Start() {
	fmt.Println("Workers are running...")

	ch := config.GetRabbitMQChannel()
	defer ch.Close()

	q, err := ch.QueueDeclare(
		"log_msg_queue",
		true,
		false,
		false,
		false,
		nil,
	)
	if err != nil {
		fmt.Printf("ошибка при объявлении очереди: %v\n", err)
		return
	}

	msgs, err := ch.Consume(
		q.Name,
		"",
		true,
		false,
		false,
		false,
		nil,
	)
	if err != nil {
		fmt.Printf("ошибка при подписке на очередь: %v\n", err)
		return
	}

	for msg := range msgs {
		var logMessage models.CreateMessage
		err := json.Unmarshal(msg.Body, &logMessage)
		if err != nil {
			fmt.Printf("ошибка при разборе сообщения из очереди: %v\n", err)
			continue
		}

		_, err = services.AddLogMessage(config.GetPostgresDB(), logMessage)
		if err != nil {
			fmt.Printf("ошибка при сохранении сообщения в базе данных: %v\n", err)
			continue
		}

		fmt.Println("Worker processed a message:", logMessage)
	}
}
