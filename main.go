package main

import (
	"github.com/aanatyrnal/mind/config"
	"github.com/aanatyrnal/mind/routes"
	"github.com/aanatyrnal/mind/workers"
	"github.com/joho/godotenv"
	"log"
	"os"
)

func loadEnvVariables() {
	if err := godotenv.Load(); err != nil {
		log.Fatalf("Error loading .env file: %v", err)
	}
}

func main() {
	loadEnvVariables()

	dbHost := os.Getenv("DB_HOST")
	dbPort := os.Getenv("DB_PORT")
	dbUser := os.Getenv("DB_USER")
	dbPassword := os.Getenv("DB_PASSWORD")
	dbName := os.Getenv("DB_NAME")

	rabbitMQHost := os.Getenv("RABBITMQ_HOST")
	rabbitMQPort := os.Getenv("RABBITMQ_PORT")
	rabbitMQUser := os.Getenv("RABBITMQ_USER")
	rabbitMQPassword := os.Getenv("RABBITMQ_PASSWORD")

	err := config.ConnectToPostgres(dbHost, dbPort, dbUser, dbPassword, dbName)
	if err != nil {
		log.Fatalf("Error connecting to PostgreSQL: %v", err)
	}
	defer config.GetPostgresDB().Close()

	err = config.ConnectToRabbitMQ(rabbitMQHost, rabbitMQPort, rabbitMQUser, rabbitMQPassword)
	if err != nil {
		log.Fatalf("Error connecting to RabbitMQ: %v", err)
	}
	defer config.GetRabbitMQChannel().Close()

	go workers.Start()

	r := routes.SetupRoutes(config.GetPostgresDB())

	r.Run(":8000")
}
