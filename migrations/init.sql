CREATE DATABASE logs;
\c logs;

--createdb -U postgres logs;
-- psql -U postgres -d logs
CREATE TABLE IF NOT EXISTS logs (
    id                  serial primary key,
    id_object           int not null,
    message             varchar not null,
    created_at          timestamp default now()
);
