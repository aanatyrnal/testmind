package services

import (
	"database/sql"
	"fmt"
	"github.com/aanatyrnal/mind/models"
	"github.com/aanatyrnal/mind/sql_queries"
)

func AddLogMessage(db *sql.DB, msg models.CreateMessage) (*models.Message, error) {
	var message models.Message
	err := db.QueryRow(sql_queries.AddMsg, msg.ObjectID, msg.Message).Scan(&message.ID, &message.ObjectID, &message.Message, &message.CreatedAt)
	if err != nil {
		return nil, fmt.Errorf("ошибка при добавлении сообщения в лог и получении добавленной записи: %v", err)
	}

	return &message, nil
}

func ReadLogMsg(db *sql.DB, idObject int) ([]models.Message, error) {
	rows, err := db.Query(sql_queries.ReadMsg, idObject)
	if err != nil {
		return nil, fmt.Errorf("ошибка при получении списка записей: %v", err)
	}
	defer rows.Close()

	var logMessages []models.Message
	for rows.Next() {
		var msg models.Message
		err := rows.Scan(&msg.ID, &msg.ObjectID, &msg.Message, &msg.CreatedAt)
		if err != nil {
			return nil, fmt.Errorf("ошибка при сканировании строки: %v", err)
		}
		logMessages = append(logMessages, msg)
	}

	if err := rows.Err(); err != nil {
		return nil, fmt.Errorf("ошибка при обработке результата: %v", err)
	}

	return logMessages, nil
}
