package services

import (
	"encoding/json"
	"fmt"
	"github.com/aanatyrnal/mind/config"
	"github.com/aanatyrnal/mind/models"
	"github.com/streadway/amqp"
)

func SendToQueue(msg models.Message) error {
	data, err := json.Marshal(msg)
	if err != nil {
		return fmt.Errorf("ошибка при преобразовании сообщения в JSON: %v", err)
	}

	ch := config.GetRabbitMQChannel()

	queueName := "log_msg_queue"
	q, err := ch.QueueDeclare(
		queueName,
		true,
		false,
		false,
		false,
		nil,
	)
	if err != nil {
		return fmt.Errorf("ошибка при объявлении очереди: %v", err)
	}

	err = ch.Publish(
		"",
		q.Name,
		false,
		false,
		amqp.Publishing{
			ContentType: "application/json",
			Body:        data,
		},
	)
	if err != nil {
		return fmt.Errorf("ошибка при публикации сообщения в очередь: %v", err)
	}

	return nil
}

func SendMsgToQueue(msgList []models.Message) error {
	ch := config.GetRabbitMQChannel()

	queueName := "log_msg_queue"
	q, err := ch.QueueDeclare(
		queueName,
		true,
		false,
		false,
		false,
		nil,
	)
	if err != nil {
		return fmt.Errorf("ошибка при объявлении очереди: %v", err)
	}

	for _, msg := range msgList {
		data, err := json.Marshal(msg)
		if err != nil {
			return fmt.Errorf("ошибка при преобразовании сообщения в JSON: %v", err)
		}

		err = ch.Publish(
			"",
			q.Name,
			false,
			false,
			amqp.Publishing{
				ContentType: "application/json",
				Body:        data,
			},
		)
		if err != nil {
			return fmt.Errorf("ошибка при публикации сообщения в очередь: %v", err)
		}
	}

	return nil
}
