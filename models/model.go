package models

import "time"

type CreateMessage struct {
	//ID        int       `json:"id"`
	ObjectID int    `json:"id_object"`
	Message  string `json:"message"`
	//CreatedAt time.Time `json:"created_at"`
}

type Message struct {
	ID        int       `json:"id"`
	ObjectID  int       `json:"id_object"`
	Message   string    `json:"message"`
	CreatedAt time.Time `json:"created_at"`
}

type LogMessage struct {
	ID        int       `json:"id"`
	ObjectID  int       `json:"object_id"`
	Message   string    `json:"message"`
	CreatedAt time.Time `json:"created_at"`
}
