package config

import (
	"fmt"

	"github.com/streadway/amqp"
)

var rabbitMQChannel *amqp.Channel

func ConnectToRabbitMQ(rabbitMQHost, rabbitMQPort, rabbitMQUser, rabbitMQPassword string) error {
	conn, err := amqp.Dial(fmt.Sprintf("amqp://%s:%s@%s:%s/", rabbitMQUser, rabbitMQPassword, rabbitMQHost, rabbitMQPort))
	if err != nil {
		return err
	}

	ch, err := conn.Channel()
	if err != nil {
		return err
	}
	rabbitMQChannel = ch
	return nil
}

func GetRabbitMQChannel() *amqp.Channel {
	return rabbitMQChannel
}
