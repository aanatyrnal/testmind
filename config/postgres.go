package config

import (
	"database/sql"
	"fmt"

	_ "github.com/lib/pq"
)

var postgresDB *sql.DB

func ConnectToPostgres(dbHost, dbPort, dbUser, dbPassword, dbName string) error {
	connStr := fmt.Sprintf("postgres://%s:%s@%s:%s/%s?sslmode=disable", dbUser, dbPassword, dbHost, dbPort, dbName)
	db, err := sql.Open("postgres", connStr)
	if err != nil {
		return err
	}
	postgresDB = db
	return nil
}

func GetPostgresDB() *sql.DB {
	return postgresDB
}
