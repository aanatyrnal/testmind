package sql_queries

const (
	AddMsg = `
		INSERT INTO logs (id_object, message, created_at)
		VALUES ($1, $2, now())
		returning id, id_object, message, created_at ;
	`

	ReadMsg = `
		SELECT id, id_object, message, created_at 
		from logs 
		where id_object = $1
		order by created_at DESC;
	`
)
