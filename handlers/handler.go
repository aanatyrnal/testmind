package handlers

import (
	"database/sql"
	"github.com/aanatyrnal/mind/models"
	"github.com/aanatyrnal/mind/services"
	"github.com/gin-gonic/gin"
	"log"
	"net/http"
	"strconv"
)

func AddLogMessageHandler(db *sql.DB) gin.HandlerFunc {
	return func(c *gin.Context) {
		var msg models.CreateMessage

		if err := c.BindJSON(&msg); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": "неверный формат данных"})
			return
		}

		message, err := services.AddLogMessage(db, msg)
		if err != nil {
			log.Printf("ошибка при сохранении сообщения в базе данных: %v", err)
			c.JSON(http.StatusInternalServerError, gin.H{"error": "ошибка при обработке запроса"})
			return
		}

		err = services.SendToQueue(*message)
		if err != nil {
			log.Printf("ошибка при отправке сообщения в очередь: %v", err)
			c.JSON(http.StatusInternalServerError, gin.H{"error": "ошибка при обработке запроса"})
			return
		}

		c.JSON(http.StatusCreated, message)
	}
}

func ReadLogMsgHandler(db *sql.DB) gin.HandlerFunc {
	return func(c *gin.Context) {
		idObjectStr := c.Param("id_object")
		idObject, err := strconv.Atoi(idObjectStr)
		if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": "неверный формат идентификатора объекта"})
			return
		}

		logMessages, err := services.ReadLogMsg(db, idObject)
		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}

		//err = services.SendMsgToQueue(logMessages)
		//if err != nil {
		//	log.Printf("ошибка при отправке сообщения в очередь: %v", err)
		//	c.JSON(http.StatusInternalServerError, gin.H{"error": "ошибка при обработке запроса"})
		//	return
		//}

		c.JSON(http.StatusOK, logMessages)
	}
}
