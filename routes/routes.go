package routes

import (
	"database/sql"
	"github.com/aanatyrnal/mind/handlers"
	"github.com/gin-gonic/gin"
)

func SetupRoutes(db *sql.DB) *gin.Engine {
	r := gin.Default()

	r.POST("/logs", handlers.AddLogMessageHandler(db))
	r.GET("/logs/:id_object", handlers.ReadLogMsgHandler(db))

	return r
}
